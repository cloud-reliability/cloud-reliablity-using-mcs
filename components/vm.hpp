#ifndef VM_H_
#define VM_H_

class VM {
	public:
		VM();
        VM(std::string n, int c, int m, int h, int b){
            _name = n;
            _cpu = c;
            _mem = m;
            _hdd = h;
            _bw = b;
        }

        int cpu(){ return _cpu;}
        int hdd(){ return _hdd;}
        int mem(){ return _mem;}
        int bw(){ return _bw;}

        void cpu(int n) { _cpu=n; }
        void hdd(int n) { _hdd=n; }
        void mem(int n) { _mem=n; }
        void bw(int n)  { _bw=n; }

    private:
        std::string _name;
        int _cpu;
        int _hdd;
        int _mem;
        int _bw;
};

#endif 
