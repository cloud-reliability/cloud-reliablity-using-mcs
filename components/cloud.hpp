#ifndef CLOUD_HPP_
#define CLOUD_HPP_

#include "cluster.hpp"
#include <vector>

class Cloud {
	public:
		Cloud();
        ~Cloud(){
            clusters.clear();
        }
    private:
        std::vector<Cluster> clusters;
};

#endif