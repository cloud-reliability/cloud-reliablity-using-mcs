// Annual Failure Rates (AFR)
// CPU .02
// Mem .01
// HDD .08
// BW  .01

// Small Test Bed
// Available
//     CPU 400
//     MEM 400
//     HDD 10,000
//     BW  50,000

// Each VM
//     CPU 2
//     MEM 2
//     HDD 100
//     BW  100

#include "CStopWatch.h"
#include <vector>
#include <random>
#include <iostream>
#include <algorithm>

#include "../Classifiers/BasicClassifier.hpp"
#include "../Classifiers/GreedyClassifier.hpp"
#include "cloud.hpp"
#include "cluster.hpp"
#include "vmGroup.hpp"

std::random_device rd;                          // only used once to initialise (seed) engine
static std::mt19937 gen(clock());
std::uniform_real_distribution<double> dis(0, 1);

double smallTestBed(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int cpuAvailable, memAvailable, hddAvailable, bwAvailable;
    int cpuRx, memRx, hddRx, bwRx;
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 400, 400, 10000, 50000));

    std::vector<VM> vmList;
    vmList.push_back(VM("", 2, 2, 100, 100)); 

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));

    cpuRx = 0; memRx = 0; hddRx = 0; bwRx = 0; 
    for(auto &it : vmGroups){
        int i=0;
        for(auto &vm: it.VMs()){
            cpuRx += vm.cpu() * (it.totalVms()*it.probs()[i]);
            memRx += vm.mem() * (it.totalVms()*it.probs()[i]);
            hddRx += vm.hdd() * (it.totalVms()*it.probs()[i]);
            bwRx  += vm.bw () * (it.totalVms()*it.probs()[i]);
            i++;
        }
    }

    while (true){

        cpuAvailable = 0;
        memAvailable = 0;
        hddAvailable = 0;
        bwAvailable = 0;
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();
        // newValue = -ln(1-u_i)/AFR
       for(unsigned int i=0; i< clusters.size(); i++){
            if(-log(1-dis(gen))/cpuAFR >= cpuAFR ){ cpuAvailable += clusters[i].cpu()+clusters[i].cpu()*osFactor; }
            if(-log(1-dis(gen))/memAFR >= memAFR ){ memAvailable += clusters[i].mem()+clusters[i].mem()*osFactor; }
            if(-log(1-dis(gen))/hddAFR >= hddAFR ){ hddAvailable += clusters[i].hdd(); }
            if(-log(1-dis(gen))/bwAFR  >= bwAFR ) { bwAvailable  += clusters[i].bw() +clusters[i].bw()*osFactor;  }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        // Classify Here
        if(cpuAvailable < cpuRx){ failed = true;}
        if(memAvailable < memRx){ failed = true;}
        if(hddAvailable < hddRx){ failed = true;}
        if(bwAvailable  < bwRx) { failed = true;}
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

        /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    // std::cout << "98 VM \n";
    // std::cout << "----------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();

    if(printOutput){
        std::cout << "98,Original," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double smallTestBed_Greedy(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 200, 200, 5000, 25000));
    clusters.push_back(Cluster("", 200, 200, 5000, 25000));

    //  Prototypes for VMs that will be used
    std::vector<VM> vmList;
    vmList.push_back(VM("", 2, 2, 100, 100)); 

    // A
    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));

    // Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){

            cluster.cpu(cluster.cpu()*(1.0+osFactor));
            cluster.mem(cluster.mem()*(1.0+osFactor));
            // cluster.hdd(cluster.hdd()*(1.0+osFactor));
            cluster.bw (cluster.bw ()*(1.0+osFactor));

            // bool r = false;
            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        
        // std::cout << "Allocating \n";
        bool allocated;

        for(auto& vm:allVms){
            allocated = false;
            for (auto &cluster : tempClusters){
                if (cluster.cpu() >= vm.cpu() && cluster.hdd() >= vm.hdd() &&cluster.mem() >= vm.mem() && cluster.bw() >= vm.bw()){
                    cluster.cpu(cluster.cpu() - vm.cpu());
                    cluster.hdd(cluster.hdd() - vm.hdd());
                    cluster.mem(cluster.mem() - vm.mem());
                    cluster.bw(cluster.bw() - vm.bw());
                    allocated = true;
                    break;
                }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }

        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    // std::cout << "98 VM \n";
    // std::cout << "----------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();

    if(printOutput){
        std::cout << "98,Greedy," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime()<< "," << numVms << "," << osFactor << "\n";
    }
    return 1-F;
}

double smallTestBed_RR(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 200, 200, 5000, 25000));
    clusters.push_back(Cluster("", 200, 200, 5000, 25000));

    //  Prototypes for VMs that will be used
    std::vector<VM> vmList;
    vmList.push_back(VM("", 2, 2, 100, 100)); 

    // A
    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));

    // Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu()*(1.0+osFactor));
            cluster.mem(cluster.mem()*(1.0+osFactor));
            // cluster.hdd(cluster.hdd()*(1.0+osFactor));
            cluster.bw (cluster.bw ()*(1.0+osFactor));

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }

        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        bool allocated;
        int curCluster = 0;

        for(auto& vm:allVms){
            allocated = false;
            for(unsigned int i=0; i<tempClusters.size(); i++){
                // Cluster cluster = tempClusters[curCluster++];

        
                if (tempClusters[curCluster].cpu() >= vm.cpu() && tempClusters[curCluster].hdd() >= vm.hdd() && 
                    tempClusters[curCluster].mem() >= vm.mem() && tempClusters[curCluster].bw() >= vm.bw()){

                    tempClusters[curCluster].cpu(tempClusters[curCluster].cpu() - vm.cpu());
                    tempClusters[curCluster].hdd(tempClusters[curCluster].hdd() - vm.hdd());
                    tempClusters[curCluster].mem(tempClusters[curCluster].mem() - vm.mem());
                    tempClusters[curCluster].bw( tempClusters[curCluster].bw() - vm.bw());
                    allocated = true;
                }

                curCluster++;
                if(curCluster == (int)tempClusters.size()){ curCluster = 0;}

                if(allocated) { break; }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        // std::cout << sigma <<  " " << numSamples << "\n";
        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    // std::cout << "98 VM \n";
    // std::cout << "----------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "98,RR," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double testBed98(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int cpuAvailable, memAvailable, hddAvailable, bwAvailable;
    int cpuRx, memRx, hddRx, bwRx;
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 400, 400, 20000, 50000));

    std::vector<VM> vmList;
    vmList.push_back((VM){"", 2, 2, 100, 100}); 

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));
    cpuRx = 0; memRx = 0; hddRx = 0; bwRx = 0; 
    for(auto &it : vmGroups){
        int i=0;
        for(auto &vm: it.VMs()){
            cpuRx += vm.cpu() * (it.totalVms()*it.probs()[i]);
            memRx += vm.mem() * (it.totalVms()*it.probs()[i]);
            hddRx += vm.hdd() * (it.totalVms()*it.probs()[i]);
            bwRx  += vm.bw()  * (it.totalVms()*it.probs()[i]);
            i++;
        }
    }
    // std::cout << "cpu"
    while(true){

        cpuAvailable = 0;
        memAvailable = 0;
        hddAvailable = 0;
        bwAvailable = 0;
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();
        // newValue = -ln(1-u_i)/AFR
        for(unsigned int i=0; i< clusters.size(); i++){
            if(-log(1-dis(gen))/cpuAFR >= cpuAFR ){ cpuAvailable += clusters[i].cpu()+clusters[i].cpu()*osFactor; }
            if(-log(1-dis(gen))/memAFR >= memAFR ){ memAvailable += clusters[i].mem()+clusters[i].mem()*osFactor; }
            if(-log(1-dis(gen))/hddAFR >= hddAFR ){ hddAvailable += clusters[i].hdd(); }
            if(-log(1-dis(gen))/bwAFR  >= bwAFR ) { bwAvailable  += clusters[i].bw() +clusters[i].bw()*osFactor;  }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        // Classify Here
        
        if(cpuAvailable < cpuRx){ failed = true;}
        if(memAvailable < memRx){ failed = true;}
        if(hddAvailable < hddRx){ failed = true;}
        if(bwAvailable  < bwRx) { failed = true;}
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if(sigma < tolerance) { break;}
    }
    
    // std::cout << "98 VM Increased HD \n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "98_Increased,Original," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double testBed98_Greedy(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 400, 400, 20000, 50000));

    std::vector<VM> vmList;
    vmList.push_back((VM){"", 2, 2, 100, 100});

    // A
    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));

    // Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu()*(1.0+osFactor));
            cluster.mem(cluster.mem()*(1.0+osFactor));
            // cluster.hdd(cluster.hdd()*(1.0+osFactor));
            cluster.bw (cluster.bw ()*(1.0+osFactor));

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        
        // std::cout << "Allocating \n";
        bool allocated;

        for(auto& vm:allVms){
            allocated = false;
            for (auto &cluster : tempClusters){
                if (cluster.cpu() >= vm.cpu() && cluster.hdd() >= vm.hdd() &&cluster.mem() >= vm.mem() && cluster.bw() >= vm.bw()){
                    cluster.cpu(cluster.cpu() - vm.cpu());
                    cluster.hdd(cluster.hdd() - vm.hdd());
                    cluster.mem(cluster.mem() - vm.mem());
                    cluster.bw(cluster.bw() - vm.bw());
                    allocated = true;
                    break;
                }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }

        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    // std::cout << "98 VM \n";
    // std::cout << "----------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "98_Increased,Greedy," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double testBed98_RR(double osFactor=0.0, int numVms = 98, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 400, 400, 20000, 50000));

    std::vector<VM> vmList;
    vmList.push_back((VM){"", 2, 2, 100, 100});

    // A
    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {1}, numVms));

    // Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu()*(1.0+osFactor));
            cluster.mem(cluster.mem()*(1.0+osFactor));
            // cluster.hdd(cluster.hdd()*(1.0+osFactor));
            cluster.bw (cluster.bw ()*(1.0+osFactor));

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        bool allocated;
        int curCluster = 0;

        for(auto& vm:allVms){
            allocated = false;
            for(unsigned int i=0; i<tempClusters.size(); i++){
                // Cluster cluster = tempClusters[curCluster++];

        
                if (tempClusters[curCluster].cpu() >= vm.cpu() && tempClusters[curCluster].hdd() >= vm.hdd() && 
                    tempClusters[curCluster].mem() >= vm.mem() && tempClusters[curCluster].bw() >= vm.bw()){

                    tempClusters[curCluster].cpu(tempClusters[curCluster].cpu() - vm.cpu());
                    tempClusters[curCluster].hdd(tempClusters[curCluster].hdd() - vm.hdd());
                    tempClusters[curCluster].mem(tempClusters[curCluster].mem() - vm.mem());
                    tempClusters[curCluster].bw( tempClusters[curCluster].bw() - vm.bw());
                    allocated = true;
                }

                curCluster++;
                if(curCluster == (int)tempClusters.size()){ curCluster = 0;}

                if(allocated) { break; }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();
         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    // std::cout << "98 VM \n";
    // std::cout << "----------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();

    if(printOutput){
        std::cout << "98_Increased,RR," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

// 21,567 VMs
/*The VM instances used in this simulation were the EC2
instances: M1 Small, M1 Medium, and M1 Large, at distribution percentages of 20%,
40%, and 40%, respectively.
*/
// M1 Small  2 2 160  100 (20%) 4313
// M1 Medium 4 4 410  500 (40%) 8626
// M1 Large  8 8 850 1000 (40%) 8626
double xsedeOne(double osFactor=0.0, int numVms = 21567, bool printOutput=true){
    int cpuAvailable, memAvailable, hddAvailable, bwAvailable;
    int cpuRx, memRx, hddRx, bwRx;
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976,     125952, 1810560,    3936000));
    clusters.push_back(Cluster("", 128,       512,    8000,       32000));
    clusters.push_back(Cluster("", 768,       3072,   256000,     640000));
    clusters.push_back(Cluster("", 112896,    150528, 2455488,    9408000));
    clusters.push_back(Cluster("", 22656,     45312,  275648,     18880000));
    clusters.push_back(Cluster("", 7144,      28576,  446500,     893000));
    clusters.push_back(Cluster("", 16384,     65536,  4096000,    102400000));
    clusters.push_back(Cluster("", 10368,     20736,  143208,     3240000));
    clusters.push_back(Cluster("", 896,       3584,   2128,       112000));
    clusters.push_back(Cluster("", 102400,    204800, 320000,     87296000));
    clusters.push_back(Cluster("", 1024,      32768,  150000,     15000));
    clusters.push_back(Cluster("", 4224,      8448,   1761144,    3600960));

    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S", 2,   2,   160, 100));  
    vmList.push_back(VM("M1 M", 4,   4,   410, 500)); 
    vmList.push_back(VM("M1 L", 8,   8,   850, 1000)); 
    
    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.2, .4, .4}, numVms));

    
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    cpuRx = 0; memRx = 0; hddRx = 0; bwRx = 0; 
    for(auto &it : vmGroups){
        int i=0;
        for(auto &vm: it.VMs()){
            cpuRx += vm.cpu() * (it.totalVms()*it.probs()[i]);
            memRx += vm.mem() * (it.totalVms()*it.probs()[i]);
            hddRx += vm.hdd() * (it.totalVms()*it.probs()[i]);
            bwRx  += vm.bw()  * (it.totalVms()*it.probs()[i]);
            i++;
        }
    }
    while(true){

        cpuAvailable = 0;
        memAvailable = 0;
        hddAvailable = 0;
        bwAvailable  = 0;
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();
        for(unsigned int i=0; i< clusters.size(); i++){
            if(-log(1-dis(gen))/cpuAFR >= cpuAFR ){ cpuAvailable += clusters[i].cpu()+clusters[i].cpu()*osFactor; }
            if(-log(1-dis(gen))/memAFR >= memAFR ){ memAvailable += clusters[i].mem()+clusters[i].mem()*osFactor; }
            if(-log(1-dis(gen))/hddAFR >= hddAFR ){ hddAvailable += clusters[i].hdd(); }
            if(-log(1-dis(gen))/bwAFR  >= bwAFR ) { bwAvailable  += clusters[i].bw() +clusters[i].bw()*osFactor;  }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        // Classify Here

        if (cpuAvailable < cpuRx) { failed = true; }
        if (memAvailable < memRx) { failed = true; }
        if (hddAvailable < hddRx) { failed = true; }
        if (bwAvailable < bwRx)   { failed = true; }
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

        /******************************************** Convergence ********************************************/
        numSamples++;
        iterations++;
        if (failed)
        {
            sumX++;
            sumXSquared++;
        }

        F = sumX / numSamples;
        vF = (1.0 / numSamples) * (sumXSquared / numSamples - pow(F, 2.0));
        sigma = sqrt(vF) / F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if (sigma < tolerance)
        {
            break;
        }
    }
    
    // std::cout << "XSEDE \n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_One,Original," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double xsedeOne_Greedy(double osFactor=0.0, int numVms = 21567, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;
    CStopWatch timer, timer1;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976, 125952, 1810560, 3936000));
    clusters.push_back(Cluster("", 128, 512, 8000, 32000));
    clusters.push_back(Cluster("", 768, 3072, 256000, 640000));
    clusters.push_back(Cluster("", 112896, 150528, 2455488, 9408000));
    clusters.push_back(Cluster("", 22656, 45312, 275648, 18880000));
    clusters.push_back(Cluster("", 7144, 28576, 446500, 893000));
    clusters.push_back(Cluster("", 16384, 65536, 4096000, 102400000));
    clusters.push_back(Cluster("", 10368, 20736, 143208, 3240000));
    clusters.push_back(Cluster("", 896, 3584, 2128, 112000));
    clusters.push_back(Cluster("", 102400, 204800, 320000, 87296000));
    clusters.push_back(Cluster("", 1024, 32768, 150000, 15000));
    clusters.push_back(Cluster("", 4224, 8448, 1761144, 3600960));

    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S", 2, 2, 160, 100));
    vmList.push_back(VM("M1 M", 4, 4, 410, 500));
    vmList.push_back(VM("M1 L", 8, 8, 850, 1000));

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.2, .4, .4}, numVms));
    // vmGroups.push_back(VMGroup("", vmList, {.2, .4, .4}, 15000));

    
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    //Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu() + cluster.cpu()*osFactor);
            cluster.mem(cluster.mem() + cluster.mem()*osFactor);
            // cluster.hdd(cluster.hdd() + cluster.hdd()*osFactor);
            cluster.bw (cluster.bw () + cluster.bw()*osFactor);

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        
        // std::cout << "Allocating \n";
        bool allocated;

        for(auto& vm:allVms){
            allocated = false;
            for (auto &cluster : tempClusters){
                if (cluster.cpu() >= vm.cpu() && cluster.hdd() >= vm.hdd() &&cluster.mem() >= vm.mem() && cluster.bw() >= vm.bw()){
                    cluster.cpu(cluster.cpu() - vm.cpu());
                    cluster.hdd(cluster.hdd() - vm.hdd());
                    cluster.mem(cluster.mem() - vm.mem());
                    cluster.bw(cluster.bw() - vm.bw());
                    allocated = true;
                    break;
                }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }

        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }

    // std::cout << "XSEDE \n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_One,Greedy," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double xsedeOne_RR(double osFactor=0.0, int numVms = 21567, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;
    CStopWatch timer, timer1;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976, 125952, 1810560, 3936000));
    clusters.push_back(Cluster("", 128, 512, 8000, 32000));
    clusters.push_back(Cluster("", 768, 3072, 256000, 640000));
    clusters.push_back(Cluster("", 112896, 150528, 2455488, 9408000));
    clusters.push_back(Cluster("", 22656, 45312, 275648, 18880000));
    clusters.push_back(Cluster("", 7144, 28576, 446500, 893000));
    clusters.push_back(Cluster("", 16384, 65536, 4096000, 102400000));
    clusters.push_back(Cluster("", 10368, 20736, 143208, 3240000));
    clusters.push_back(Cluster("", 896, 3584, 2128, 112000));
    clusters.push_back(Cluster("", 102400, 204800, 320000, 87296000));
    clusters.push_back(Cluster("", 1024, 32768, 150000, 15000));
    clusters.push_back(Cluster("", 4224, 8448, 1761144, 3600960));

    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S", 2, 2, 160, 100));
    vmList.push_back(VM("M1 M", 4, 4, 410, 500));
    vmList.push_back(VM("M1 L", 8, 8, 850, 1000));

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.2, .4, .4}, numVms));
    // vmGroups.push_back(VMGroup("", vmList, {.2, .4, .4}, 15000));

    
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    //Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu() + cluster.cpu()*osFactor);
            cluster.mem(cluster.mem() + cluster.mem()*osFactor);
            // cluster.hdd(cluster.hdd() + cluster.hdd()*osFactor);
            cluster.bw (cluster.bw () + cluster.bw()*osFactor);

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        bool allocated;
        int curCluster = 0;

        for(auto& vm:allVms){
            allocated = false;
            for(unsigned int i=0; i<tempClusters.size(); i++){
                // Cluster cluster = tempClusters[curCluster++];

        
                if (tempClusters[curCluster].cpu() >= vm.cpu() && tempClusters[curCluster].hdd() >= vm.hdd() && 
                    tempClusters[curCluster].mem() >= vm.mem() && tempClusters[curCluster].bw() >= vm.bw()){

                    tempClusters[curCluster].cpu(tempClusters[curCluster].cpu() - vm.cpu());
                    tempClusters[curCluster].hdd(tempClusters[curCluster].hdd() - vm.hdd());
                    tempClusters[curCluster].mem(tempClusters[curCluster].mem() - vm.mem());
                    tempClusters[curCluster].bw( tempClusters[curCluster].bw() - vm.bw());
                    allocated = true;
                }

                curCluster++;
                if(curCluster == (int)tempClusters.size()){ curCluster = 0;}

                if(allocated) { break; }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();
         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }

    // std::cout << "XSEDE \n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_One,RR," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}
//13,000 VMs
/*The VM instances used in this simulation were the EC2
instances: M1 Small, M1 Medium, and M1 Large, at distribution percentages of 20%,
40%, and 40%, respectively.
*/
// M1 Small         2 2 160  100        (41%) 
// M1 Medium        4 4 410  500        (20%) 
// M1 Large         8 8 850 1000        (10%) 
// M1 XL            16 16 1,690 1,000   (8%)
// M3 XL            26 16 1,690 1,000   (4%)
// M3 2XL           52 32 3,380 2,000   (1%)
// High Mem XL      13 18 420 500       (1%)
// High Mem 2XL     26 35 850 1,000     (1%)
// High Mem 4XL     52 70 1,690 1,000   (2%)
// High CPU Medium  10 2 350 500        (1%)
// High CPU XL      40 8 1,690 1,000    (1%)
// Cluster 8XL      176 64 3,370 10,000 (10%)
double xsedeTwo(double osFactor=0.0, int numVms = 13000, bool printOutput=true){
    int cpuAvailable, memAvailable, hddAvailable, bwAvailable;
    int cpuRx, memRx, hddRx, bwRx;
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;

    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;
    
    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR  = 0.01;

    std::vector<double> vFs, Fs, sigmas;

    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976,  125952, 1810560,  3936000));
    clusters.push_back(Cluster("", 128,    512,    8000,     32000));
    clusters.push_back(Cluster("", 768,    3072,   256000,   640000));
    clusters.push_back(Cluster("", 112896, 150528, 2455488,  9408000));
    clusters.push_back(Cluster("", 22656,  45312,  275648,   18880000));
    clusters.push_back(Cluster("", 7144,   28576,  446500,   893000));
    clusters.push_back(Cluster("", 16384,  65536,  4096000,  102400000));
    clusters.push_back(Cluster("", 10368,  20736,  143208,   3240000));
    clusters.push_back(Cluster("", 896,    3584,   2128,     112000));
    clusters.push_back(Cluster("", 102400, 204800, 320000,   87296000));
    clusters.push_back(Cluster("", 1024,   32768,  150000,   15000));
    clusters.push_back(Cluster("", 4224,   8448,   1761144,  3600960));

    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S",                 2,   2,   160, 100));  
    vmList.push_back(VM("M1 M",                 4,   4,   410, 500));  
    vmList.push_back(VM("M1 L",                 8,   8,   850, 1000)); 
    vmList.push_back(VM("M1 XL",               16,  16, 1690, 1000));  
    vmList.push_back(VM("M3 XL",               26,  16, 1690, 1000));  
    vmList.push_back(VM("M3 2XL",              52,  32, 3380, 2000));  
    vmList.push_back(VM("High Memory XL",      13,  18,  420, 500));
    vmList.push_back(VM("High Memory 2XL",     26,  35,  850, 1000));
    vmList.push_back(VM("High Memory 4XL",     52,  70,  690, 1000));
    vmList.push_back(VM("High CPU M",          10,  2,   350, 500));
    vmList.push_back(VM("High CPU XL",         40,  8,  1690, 1000));
    vmList.push_back(VM("Cluster Compute 8XL", 176, 64, 3370, 10000));

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.41, .2, .10, .08, .04, .01, .01, .01, .02, .01, .01, .1}, numVms));

    cpuRx = 0; memRx = 0; hddRx = 0; bwRx = 0; 
    for(auto &it : vmGroups){
        int i=0;
        for(auto &vm: it.VMs()){
            cpuRx += vm.cpu() * (it.totalVms()*it.probs()[i]);
            memRx += vm.mem() * (it.totalVms()*it.probs()[i]);
            hddRx += vm.hdd() * (it.totalVms()*it.probs()[i]);
            bwRx  += vm.bw()  * (it.totalVms()*it.probs()[i]);
            i++;
        }
    }

    while(true){

        /*cpuRx  = 322270;   */cpuAvailable = 0;
        /*memRx  = 170170;   */memAvailable = 0;
        /*hddRx  = 11350300; */hddAvailable = 0;
        /*bwRx   = 18603000; */bwAvailable  = 0;
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();
        for(unsigned int i=0; i< clusters.size(); i++){
            if(-log(1-dis(gen))/cpuAFR >= cpuAFR ){ cpuAvailable += clusters[i].cpu()+clusters[i].cpu()*osFactor; }
            if(-log(1-dis(gen))/memAFR >= memAFR ){ memAvailable += clusters[i].mem()+clusters[i].mem()*osFactor; }
            if(-log(1-dis(gen))/hddAFR >= hddAFR ){ hddAvailable += clusters[i].hdd(); }
            if(-log(1-dis(gen))/bwAFR  >= bwAFR ) { bwAvailable  += clusters[i].bw() +clusters[i].bw()*osFactor;  }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        // Classify Here
        
        if(cpuAvailable < cpuRx){ failed = true;}
        if(memAvailable < memRx){ failed = true;}
        if(hddAvailable < hddRx){ failed = true;}
        if(bwAvailable  < bwRx) { failed = true;}
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if(sigma < tolerance) { break;}
    }
    
    // std::cout << "XSEDE Two\n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_Two,Original," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double xsedeTwo_Greedy(double osFactor=0.0, int numVms = 13000, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;
    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR = 0.01;


    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976,  125952, 1810560,  3936000));
    clusters.push_back(Cluster("", 128,    512,    8000,     32000));
    clusters.push_back(Cluster("", 768,    3072,   256000,   640000));
    clusters.push_back(Cluster("", 112896, 150528, 2455488,  9408000));
    clusters.push_back(Cluster("", 22656,  45312,  275648,   18880000));
    clusters.push_back(Cluster("", 7144,   28576,  446500,   893000));
    clusters.push_back(Cluster("", 16384,  65536,  4096000,  102400000));
    clusters.push_back(Cluster("", 10368,  20736,  143208,   3240000));
    clusters.push_back(Cluster("", 896,    3584,   2128,     112000));
    clusters.push_back(Cluster("", 102400, 204800, 320000,   87296000));
    clusters.push_back(Cluster("", 1024,   32768,  150000,   15000));
    clusters.push_back(Cluster("", 4224,   8448,   1761144,  3600960));


    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S",                 2,   2,   160, 100));  
    vmList.push_back(VM("M1 M",                 4,   4,   410, 500));  
    vmList.push_back(VM("M1 L",                 8,   8,   850, 1000)); 
    vmList.push_back(VM("M1 XL",               16,  16, 1690, 1000));  
    vmList.push_back(VM("M3 XL",               26,  16, 1690, 1000));  
    vmList.push_back(VM("M3 2XL",              52,  32, 3380, 2000));  
    vmList.push_back(VM("High Memory XL",      13,  18,  420, 500));
    vmList.push_back(VM("High Memory 2XL",     26,  35,  850, 1000));
    vmList.push_back(VM("High Memory 4XL",     52,  70,  690, 1000));
    vmList.push_back(VM("High CPU M",          10,  2,   350, 500));
    vmList.push_back(VM("High CPU XL",         40,  8,  1690, 1000));
    vmList.push_back(VM("Cluster Compute 8XL", 176, 64, 3370, 10000));

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.41, .2, .10, .08, .04, .01, .01, .01, .02, .01, .01, .1}, numVms));

    std::vector<double> vFs, Fs, sigmas;

    //Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu() + cluster.cpu()*osFactor);
            cluster.mem(cluster.mem() + cluster.mem()*osFactor);
            // cluster.hdd(cluster.hdd() + cluster.hdd()*osFactor);
            cluster.bw (cluster.bw () + cluster.bw()*osFactor);

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        
        // std::cout << "Allocating \n";
        bool allocated;

        for(auto& vm:allVms){
            allocated = false;
            for (auto &cluster : tempClusters){
                if (cluster.cpu() >= vm.cpu() && cluster.hdd() >= vm.hdd() &&cluster.mem() >= vm.mem() && cluster.bw() >= vm.bw()){
                    cluster.cpu(cluster.cpu() - vm.cpu());
                    cluster.hdd(cluster.hdd() - vm.hdd());
                    cluster.mem(cluster.mem() - vm.mem());
                    cluster.bw(cluster.bw() - vm.bw());
                    allocated = true;
                    break;
                }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }

        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    
    // std::cout << "XSEDE Two\n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_Two,Greedy," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

double xsedeTwo_RR(double osFactor=0.0, int numVms = 13000, bool printOutput=true){
    int numSamples = 0, iterations = 0;
    double sumX = 0, sumXSquared = 0;
    bool failed;
    CStopWatch timer, timer1;
    double stateGenerationTime = 0, classificationTime = 0,
           F = 1, vF = 0, sigma = 0, tolerance = 0.08;

    double cpuAFR = 0.02,
           memAFR = 0.01,
           hddAFR = 0.08,
           bwAFR = 0.01;


    timer.startTimer();
    std::vector<Cluster> clusters;
    clusters.push_back(Cluster("", 62976,  125952, 1810560,  3936000));
    clusters.push_back(Cluster("", 128,    512,    8000,     32000));
    clusters.push_back(Cluster("", 768,    3072,   256000,   640000));
    clusters.push_back(Cluster("", 112896, 150528, 2455488,  9408000));
    clusters.push_back(Cluster("", 22656,  45312,  275648,   18880000));
    clusters.push_back(Cluster("", 7144,   28576,  446500,   893000));
    clusters.push_back(Cluster("", 16384,  65536,  4096000,  102400000));
    clusters.push_back(Cluster("", 10368,  20736,  143208,   3240000));
    clusters.push_back(Cluster("", 896,    3584,   2128,     112000));
    clusters.push_back(Cluster("", 102400, 204800, 320000,   87296000));
    clusters.push_back(Cluster("", 1024,   32768,  150000,   15000));
    clusters.push_back(Cluster("", 4224,   8448,   1761144,  3600960));


    std::vector<VM> vmList;
    vmList.push_back(VM("M1 S",                 2,   2,   160, 100));  
    vmList.push_back(VM("M1 M",                 4,   4,   410, 500));  
    vmList.push_back(VM("M1 L",                 8,   8,   850, 1000)); 
    vmList.push_back(VM("M1 XL",               16,  16, 1690, 1000));  
    vmList.push_back(VM("M3 XL",               26,  16, 1690, 1000));  
    vmList.push_back(VM("M3 2XL",              52,  32, 3380, 2000));  
    vmList.push_back(VM("High Memory XL",      13,  18,  420, 500));
    vmList.push_back(VM("High Memory 2XL",     26,  35,  850, 1000));
    vmList.push_back(VM("High Memory 4XL",     52,  70,  690, 1000));
    vmList.push_back(VM("High CPU M",          10,  2,   350, 500));
    vmList.push_back(VM("High CPU XL",         40,  8,  1690, 1000));
    vmList.push_back(VM("Cluster Compute 8XL", 176, 64, 3370, 10000));

    std::vector<VMGroup> vmGroups;
    vmGroups.push_back(VMGroup("", vmList, {.41, .2, .10, .08, .04, .01, .01, .01, .02, .01, .01, .1}, numVms));

    std::vector<double> vFs, Fs, sigmas;

    //Get list of VMs that actually exist
    // std::cout << "Get All VMS\n";
    std::vector<VM> allVms;
    for (auto &vmgroup : vmGroups){
        for (auto &v : vmgroup.getVMs()){
            allVms.emplace_back(v);
        }
    }

    while (true){
        failed = false;

        /******************************************** Generate States********************************************/
        timer1.startTimer();

        std::vector<Cluster> tempClusters(clusters);

        // Adjust Cluster Resources
        for (auto &cluster : tempClusters){
            // bool r = false;
            cluster.cpu(cluster.cpu() + cluster.cpu()*osFactor);
            cluster.mem(cluster.mem() + cluster.mem()*osFactor);
            // cluster.hdd(cluster.hdd() + cluster.hdd()*osFactor);
            cluster.bw (cluster.bw () + cluster.bw()*osFactor);

            if(-log(1-dis(gen))/cpuAFR < cpuAFR ){ cluster.cpu(0); }
            if(-log(1-dis(gen))/memAFR < memAFR ){ cluster.mem(0); }
            if(-log(1-dis(gen))/hddAFR < hddAFR ){ cluster.hdd(0); }
            if(-log(1-dis(gen))/bwAFR  < bwAFR ) { cluster.bw (0); }
        }
        timer1.stopTimer();
        stateGenerationTime += timer1.getElapsedTime();

        /******************************************** Classify States ********************************************/
        timer1.startTimer();
        bool allocated;
        int curCluster = 0;

        for(auto& vm:allVms){
            allocated = false;
            for(unsigned int i=0; i<tempClusters.size(); i++){
                // Cluster cluster = tempClusters[curCluster++];

        
                if (tempClusters[curCluster].cpu() >= vm.cpu() && tempClusters[curCluster].hdd() >= vm.hdd() && 
                    tempClusters[curCluster].mem() >= vm.mem() && tempClusters[curCluster].bw() >= vm.bw()){

                    tempClusters[curCluster].cpu(tempClusters[curCluster].cpu() - vm.cpu());
                    tempClusters[curCluster].hdd(tempClusters[curCluster].hdd() - vm.hdd());
                    tempClusters[curCluster].mem(tempClusters[curCluster].mem() - vm.mem());
                    tempClusters[curCluster].bw( tempClusters[curCluster].bw() - vm.bw());
                    allocated = true;
                }

                curCluster++;
                if(curCluster == (int)tempClusters.size()){ curCluster = 0;}

                if(allocated) { break; }
            }

            if(!allocated){
                failed = true;
                break;
            }   
        }
        timer1.stopTimer();
        classificationTime += timer1.getElapsedTime();

         /******************************************** Convergence ********************************************/
        numSamples++; iterations++;
        if(failed){ 
            sumX++; sumXSquared++;
        }

        F  = sumX/numSamples;
        vF = (1.0/numSamples) * (sumXSquared/numSamples - pow(F,2.0));
        sigma = sqrt(vF)/F;

        vFs.push_back(vF);
        Fs.push_back(F);
        sigmas.push_back(sigma);

        if((numSamples > 10 && sigma < tolerance) || (numSamples > 20000 && 1-F> 0.999999)) { break;}
    }
    
    // std::cout << "XSEDE Two\n";
    // std::cout << "------------------\n";
    // std::cout << "Reliability:    " << 1-F << std::endl;
    // std::cout << "Total Samples:  " << numSamples << "\n";
    // std::cout << "Total Failures: " << sumX << "\n";

    timer.stopTimer();
    
    if(printOutput){
        std::cout << "XSEDE_Two,RR," << 1 - F << "," << numSamples << "," << sumX << "," << timer.getElapsedTime() << "," << numVms << "," << osFactor << "\n";
    }

    return 1-F;
}

int main(){

    int numTrials = 10;

    /************************************** Experiment #1 **************************************/
    /************ Original Allocation ************/
    // for (int i = 0; i < numTrials; i++){ smallTestBed();}
    // for (int i = 0; i < numTrials; i++){ testBed98(); }
    // for (int i = 0; i < numTrials; i++){ xsedeOne(); }
    // for (int i = 0; i < numTrials; i++){ xsedeTwo(); }

    // /************ Greedy Allocation ************/    
    // for (int i = 0; i < numTrials; i++){ smallTestBed_Greedy();}
    // for (int i = 0; i < numTrials; i++){ testBed98_Greedy(); }
    // for (int i = 0; i < numTrials; i++){ xsedeOne_Greedy(); }
    // for (int i = 0; i < numTrials; i++){ xsedeTwo_Greedy(); }

    // /************ Round Robin Allocation ************/
    // for (int i = 0; i < numTrials; i++){ smallTestBed_RR(); }
    // for (int i = 0; i < numTrials; i++){ testBed98_RR(); }
    // for (int i = 0; i < numTrials; i++){ xsedeOne_RR(); }
    // for (int i = 0; i < numTrials; i++){ xsedeTwo_RR(); }

    // for(double osFactor=.1; osFactor<=1; osFactor+=.1){
    //     /************ Original Allocation w/ Oversubscription ************/
    //     for (int i = 0; i < numTrials; i++){ smallTestBed(osFactor);}
    //     for (int i = 0; i < numTrials; i++){ testBed98(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeOne(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeTwo(osFactor); }

    //     /************ Greedy Allocation w/ Oversubscription ************/    
    //     for (int i = 0; i < numTrials; i++){ smallTestBed_Greedy(osFactor);}
    //     for (int i = 0; i < numTrials; i++){ testBed98_Greedy(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeOne_Greedy(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeTwo_Greedy(osFactor); }

    //     /************ Round Robin Allocation w/ Oversubscription ************/
    //     for (int i = 0; i < numTrials; i++){ smallTestBed_RR(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ testBed98_RR(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeOne_RR(osFactor); }
    //     for (int i = 0; i < numTrials; i++){ xsedeTwo_RR(osFactor); }
    // }



    /************************************** Experiment #2 **************************************/
    // For failed trials, how much does the VM request need to be reduced by
    // In order to have a successfull trial?
    
    // double R;
    // for(double osFactor=0; osFactor<=1; osFactor += .1){
    //     for(int numVms=21567; numVms > 5000; numVms--){
    //         R = xsedeOne_Greedy(osFactor, numVms, false); 
    //         if(R > 0){
    //             int i=0;
    //             while(i<numTrials){
    //                 R = xsedeOne_Greedy(osFactor, numVms, false); 
    //                 if(R > 0){ i++;}
    //                 else {
    //                     numVms--;
    //                     i=0;
    //                 }
    //             }
    //             for(int i=0; i<numTrials; i++){
    //                 R = xsedeOne_Greedy(osFactor, numVms, true);
    //             }
    //             std::cout << "\n";
    //             break;
    //         }
    //     }
    // }

    // for(double osFactor=0; osFactor<=1; osFactor += .1){
    //     for(int numVms=21567; numVms > 5000; numVms--){
    //         R = xsedeOne_RR(osFactor, numVms, false); 
    //         if(R > 0){
    //             int i=0;
    //             while(i<numTrials){
    //                 R = xsedeOne_RR(osFactor, numVms, false); 
    //                 if(R > 0){ i++;}
    //                 else {
    //                     numVms--;
    //                     i=0;
    //                 }
    //             }

    //             for(int i=0; i<numTrials; i++){
    //                 R = xsedeOne_RR(osFactor, numVms, true);
    //             }
    //             std::cout << "\n";
    //             break;
    //         }
    //     }
    // }

    // for(double osFactor=0; osFactor<=1; osFactor += .1){
    //     for(int numVms=13000; numVms > 5000; numVms--){
    //         R = xsedeTwo_Greedy(osFactor, numVms, false); 
    //         if(R > 0){
    //             int i=0;
    //             while(i<numTrials){
    //                 R = xsedeTwo_Greedy(osFactor, numVms, false); 
    //                 if(R > 0){ i++;}
    //                 else {
    //                     numVms--;
    //                     i=0;
    //                 }
    //             }
    //             for(int i=0; i<numTrials; i++){
    //                 R = xsedeTwo_Greedy(osFactor, numVms, true);
    //             }
    //             std::cout << "\n";
    //             break;
    //         }
    //     }
    // }
    
    // for(double osFactor=0; osFactor<=1; osFactor += .1){
    //     for(int numVms=13000; numVms > 5000; numVms--){
    //         R = xsedeTwo_RR(osFactor, numVms, false); 
    //         if(R > 0){
    //             int i=0;
    //             while(i<numTrials){
    //                 R = xsedeTwo_RR(osFactor, numVms, false); 
    //                 if(R > 0){ i++;}
    //                 else {
    //                     numVms--;
    //                     i=0;
    //                 }
    //             }
    //             for(int i=0; i<numTrials; i++){
    //                 R = xsedeTwo_RR(osFactor, numVms, true);
    //             }
    //             std::cout << "\n";
    //             break;
    //         }
    //     }
    // }

    double R;
    int numVms = 21567;
    for(double osFactor=1.1; osFactor<5; osFactor += .1){
        R = xsedeOne_Greedy(osFactor, numVms, false); 
        if(R > 0){
            int i=0;
            while(i<numTrials){
                R = xsedeOne_Greedy(osFactor, numVms, false); 
                if(R > 0){ i++;}
                else {
                    numVms--;
                    i=0;
                }
            }
            for(int i=0; i<numTrials; i++){
                R = xsedeOne_Greedy(osFactor, numVms, true);
            }
            std::cout << "\n";
            break;
        }
    }

    numVms = 21567;
    for(double osFactor=1.1; osFactor<5; osFactor += .1){
        R = xsedeOne_RR(osFactor, numVms, false); 
        if(R > 0){
            int i=0;
            while(i<numTrials){
                R = xsedeOne_RR(osFactor, numVms, false); 
                if(R > 0){ i++;}
                else {
                    numVms--;
                    i=0;
                }
            }
            for(int i=0; i<numTrials; i++){
                R = xsedeOne_RR(osFactor, numVms, true);
            }
            std::cout << "\n";
            break;
        }
    }

    numVms = 13000;
    for(double osFactor=1.1; osFactor<15; osFactor += .1){
        R = xsedeTwo_Greedy(osFactor, numVms, false); 
        if(R > 0){
            int i=0;
            while(i<numTrials){
                R = xsedeTwo_Greedy(osFactor, numVms, false); 
                if(R > 0){ i++;}
                else {
                    numVms--;
                    i=0;
                }
            }
            for(int i=0; i<numTrials; i++){
                R = xsedeTwo_Greedy(osFactor, numVms, true);
            }
            std::cout << "\n";
            break;
        }
    }
    numVms = 13000;
    for(double osFactor=1.1; osFactor<15; osFactor += .1){
        R = xsedeTwo_RR(osFactor, numVms, false); 
        if(R > 0){
            int i=0;
            while(i<numTrials){
                R = xsedeTwo_RR(osFactor, numVms, false); 
                if(R > 0){ i++;}
                else {
                    numVms--;
                    i=0;
                }
            }
            for(int i=0; i<numTrials; i++){
                R = xsedeTwo_RR(osFactor, numVms, true);
            }
            std::cout << "\n";
            break;
        }
    }

    
    return 0;
}