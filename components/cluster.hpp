#ifndef CLUSTER_HPP_
#define CLUSTER_HPP_

#include "server.hpp"
#include <vector>

// struct Cluster {
//     int cpu;
//     int mem;
//     int hdd;
//     int bw;
//     int numNodes;
//     std::string name;
// };

class Cluster {
	public:
		Cluster();
        Cluster(std::string s, int c, int m, int h, int b, int n=1){
            _name = s;
            _cpu = c;
            _mem = m;
            _hdd = h;
            _bw = b;
            _numNodes = n;
        }
        ~Cluster(){
            // servers.clear();
        }

        int mem(){ return this->_mem; }
        int cpu(){ return this->_cpu; }
        int hdd(){ return this->_hdd; }
        int bw() { return this->_bw; }

        void mem(int n) { this->_mem=n; }
        void cpu(int n) { this->_cpu=n; }
        void hdd(int n) { this->_hdd=n; }
        void bw(int n)  { this->_bw=n; }

    private:
        // std::vector<Server> servers;
        std::string _name;
        int _cpu;
        int _mem;
        int _hdd;
        int _bw;
        int _numNodes;
};

#endif