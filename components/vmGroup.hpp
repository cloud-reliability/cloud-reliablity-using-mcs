/** OpenMP Batch Sampler **/

#ifndef VMGROUP_H_
#define VMGROUP_H_

#include <vector>
#include "vm.hpp"

class VMGroup {
	public:
		VMGroup();
        VMGroup(std::string n, std::vector<VM> v, std::vector<double> p, int t, bool s=true){
            _name = n;
            _VMs = v;
            _probs = p;
            _totalVms = t;
            _staticAllocation = s;
        }

        std::vector<VM> getVMs(){
            std::vector<VM> retValue;

            // For each Type of VM
            for(unsigned int x=0; x<_VMs.size();x++){
                VM vm = _VMs[x];
                
                // Create that numbe of VMs and push back
                for (int i = 0; i < _totalVms * _probs[x]; i++){ //Push back # of identical VMs
                    retValue.push_back(VM("", vm.cpu(), vm.mem(), vm.hdd(), vm.bw()));
                }
            }
            std::random_shuffle(retValue.begin(), retValue.end());

            // std::cout << "Returning " << retValue.size() << "\n";
            return retValue;
        }

        ~VMGroup(){
            _VMs.clear();
            _probs.clear();
        }

        std::vector<VM> VMs()       { return _VMs; }
        std::vector<double> probs() {return _probs;}  
        int totalVms()              { return _totalVms;}  
    
    private:
        std::vector<VM> _VMs;
        std::string _name;
        std::vector<double> _probs;
        int _totalVms;

        bool _staticAllocation;
};

#endif 
